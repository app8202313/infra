# Infra V1

В данном репозитории содержится описание развертывания основной инфраструктуры проекта grusha.
Мы используем 2 облачных сервера от Selectel, DBaaS, а также DNS сервера от Selectel для домена grusha.fun

Был развернут gitlab community edition, 1 runner и хранилище для обзразов Harbor
(Данный репозиторий)
Настроен бэкап всего гитлаба (https://gitlab.grusha.fun/backup/gitlab-backup)

На втором сервере развернут бэкенд приложения, фронтенд, а также Traefik в качестве reverse-proxy
(https://gitlab.grusha.fun/box/infra)
Также на нашем гитлабе хранится код бэка и фронта, а также настроено cicd.
